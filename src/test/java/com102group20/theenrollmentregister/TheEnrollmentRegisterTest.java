/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com102group20.theenrollmentregister;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.util.*;
import java.io.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
/**
 *
 * @author atoman
 */
public class TheEnrollmentRegisterTest {
    
    public String testName;
    
    public void setTestName(String name){ this.testName = name; }

    @org.junit.jupiter.api.BeforeAll
    public static void setUpClass() throws Exception {
        System.out.println("Enrollment Registration Testing Log");
    }

    @org.junit.jupiter.api.AfterAll
    public static void tearDownClass() throws Exception {
        System.out.println("All test has been done.");
        System.out.println("Thanks for testing");
    }

    @org.junit.jupiter.api.BeforeEach
    public void setUp() throws Exception {
        System.out.println("============================");
        System.out.println("Test method initiated for "+ this.testName);
    }

    @org.junit.jupiter.api.AfterEach
    public void tearDown() throws Exception {
    }
    
//    @BeforeAll
//    public static void setUpClass() {
//    }
//    
//    @AfterAll
//    public static void tearDownClass() {
//    }
//    
//    @BeforeEach
//    public void setUp() {
//    }
//    
//    @AfterEach
//    public void tearDown() {
//    }

    /**
     * Test of main method, of class TheEnrollmentRegister.
     */
    @org.junit.jupiter.api.Test
    public void testMain() {
        setTestName("testMain");
        String[] args = null;
        TheEnrollmentRegister.main(args);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
        System.out.println("");
    }

    /**
     * Test of populateData method, of class TheEnrollmentRegister.
     */
    @org.junit.jupiter.api.Test
    public void testPopulateData() {
        setTestName("testPopulateData");
        TheEnrollmentRegister instance = new TheEnrollmentRegister();
        instance.populateData();
        System.out.println("");
        
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of start method, of class TheEnrollmentRegister.
     */
    @org.junit.jupiter.api.Test
    public void testStart() {
        setTestName("testStart");
        TheEnrollmentRegister instance = new TheEnrollmentRegister();
        instance.start();
        System.out.println("");
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of menu method, of class TheEnrollmentRegister.
     */
    @org.junit.jupiter.api.Test
    public void testMenu() {
        setTestName("testMenu");
        TheEnrollmentRegister instance = new TheEnrollmentRegister();
        instance.menu();
        
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
    @org.junit.jupiter.api.Test
//    @ParameterizedTest
//    //@ValueSource()
    public void addStudent(){
        System.out.println("Test Report for test method: addStudent");
        String[] myData = {"Taofik", "Akinpelu", "M", "20/10/2000", "FT", "2", "3"};
        var instance = new Student(myData[0], myData[1], myData[2].charAt(0), myData[3], myData[4], Integer.parseInt(myData[5]), Integer.parseInt(myData[5]));
        instance.addStudent();
        
        
        String returnedName = "";
        for(int i = 0; i < instance.studentArray.size(); i++){
            String[] arr = instance.studentArray.get(i).toString().split(","); 
            returnedName = arr[0].toUpperCase();
        }
        String expectedName = myData[0].toUpperCase()+" "+myData[1].toUpperCase();
        assertEquals(expectedName,returnedName );
   
        //assertTrue((expectedName == null ? returnedName == null : expectedName.equals(returnedName)));
        //fail();
    }
    
    @org.junit.jupiter.api.Test
    public void tuitionFeeForFTLevel3(){
        System.out.println("Test Report for test method: tuitionFeeForFTLevel3");
        int studentLevel = 3;
        FullTime fulltime = new FullTime(studentLevel);
        double fee = fulltime.schoolFee(); 
        
        assertEquals(2500.00, fee);
        
    }
    
    @org.junit.jupiter.api.Test
    public void tuitionFeeForFTNotLevel1(){
        System.out.println("Test Report for test method: tuitionFeeForFTNotLevel1");
        int studentLevel = 1;
        FullTime fulltime = new FullTime(studentLevel);
        double fee = fulltime.schoolFee(); 
        
        assertEquals(5000.00, fee);
        
    }
    
    @org.junit.jupiter.api.Test
    public void tuitionFeeForPT(){
        System.out.println("Test Report for test method: tuitionFeeForPT");
        int moduleNumber = 3;
        PartTime partTime = new PartTime(moduleNumber);
        double fee = partTime.schoolFee(); 
        double expectedFee= moduleNumber * 750;
        assertEquals(expectedFee, fee);
        
    }
    
}
