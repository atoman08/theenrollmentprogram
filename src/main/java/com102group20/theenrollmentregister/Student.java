/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com102group20.theenrollmentregister;

/**
 *
 * @author atoman
 * * Group Member: Akinpelu Taofik Opeyemi (B00902238) 
 * and Adebayo Rasheed Ayomide (B00857088)
 */
import java.io.*;                                    // import java kit for I/O (all)
import java.util.*;                                 // import java kit for utilities (all)
import java.nio.file.Paths;                         // Import java kit ffor Paths
import java.util.regex.*;                           // import java kit for regex
import java.time.*;                                 // import java kit for time and date

public class Student {
    private String firstName;                       // set first name variable as String
    private String lastName;                         // set last name variable as String
    private char gender;                            // set gender variable as Char
    private String dob;                             // set date of birth variable as string
    private String studyMode;                       // set study mode as string
    private int levelYear;                          // set level year as integer
    private int numberOfModule;                     // set Module numbers to integer
    private int noOfMale;                           // set number of male
    private int noOfFemale;                         // set number of female
    private int noOfFT;                             // set number of FT studnet
    private int noOfPT;                             // set number of pPT studnet
    private float fee;                              // set fee as float
    public String courseName;                       // set course name as string, to take in name of the program
    public String heading;                          // set heading title as string
    
    Scanner input = new Scanner(System.in);     // Set input Object for 
    
    public static final TheEnrollmentRegister home = new TheEnrollmentRegister();   // reinitiate a Main method as object 
    BufferedReader reader;                          // declare reader as an object with property of BufferReader
    
    //set Path link to student details file 
    String studentFile = Paths.get("").toAbsolutePath().toString()+"/src/main/java/com102group20/theenrollmentregister/StudentDetails.txt";
    // set path link to course details file
    String courseFile = Paths.get("").toAbsolutePath().toString()+"/src/main/java/com102group20/theenrollmentregister/CourseDetails.txt";
    
    
    List studentArray = new ArrayList();            //create a list to store student record
    
    public Integer MAX_STUDENT = 20;                // set Maximum value for student record in List
    
    public Student(){}                              // create a Student class constructor
    
    /**
     * Create Student Constructor with Object
     * @param inputFirstName
     * @param inputLastName
     * @param inputGender
     * @param inputDob
     * @param inputStudyMode
     * @param inputLevelYear
     * @param inputNumberOfModule 
     */ 
    public Student(String inputFirstName, String inputLastName, char inputGender, String inputDob, String inputStudyMode, int inputLevelYear, int inputNumberOfModule){
        firstName = inputFirstName;                 // store first name in variable
        lastName = inputLastName;                   // store last name in variable
        gender = inputGender;                       // store gender in variable
        dob = inputDob;                             // store date of birth in variable
        studyMode = inputStudyMode;                 // store study mode in variable
        levelYear = inputLevelYear;                 // store level year in variable
        numberOfModule = inputNumberOfModule;       // store number of module in variable
    }
    
    /**
     * void method to set First Name
     */
    public void setFirstName(){
        System.out.println("Enter First Name: ");
        firstName = input.nextLine().toUpperCase().trim();
        
        // check if first name is empty and check if only alphabet was inputed, if didnt pass call the method again
        if(firstName.length() < 1 || !Pattern.matches("^[A-Za-z\\s]*$", firstName)) this.setFirstName();
    }
    
    /**
     * void method to set last name
     */
    public void setLastName(){
        System.out.println("Enter Last Name: ");
        lastName = input.nextLine().toUpperCase().trim();
        
        // check if last name is empty and check if only alphabet was inputed, if didnt pass call the method again
        if(lastName.length() < 1 || !Pattern.matches("^[A-Za-z\\s]*$", lastName)) this.setLastName();
    }
    
    /**
     * void method to set bate of birth
     * */
    public void setDOB(){
        System.out.println("Enter Date of Birth (format: dd/mm/yyyy): ");
        dob = input.nextLine().toUpperCase().trim();
        
        // check if dob empty and and check if dob has right date format, if didnt pass call the method again
        if(dob.length() < 1 || this.checkDob() == false) this.setDOB();
    }
    
    /**
     * Void method to set Gender
     */ 
    public void setGender(){
        System.out.println("Enter Gender (M/F): ");
        gender = Character.toUpperCase(input.nextLine().charAt(0));
       
        if(gender != 'M' && gender != 'F')this.setGender();
       
    }
    
    /**
     * void method to set study mode
     */
    public void setStudyMode(){
        System.out.println("Enter study mode (FT/PT): ");
        studyMode = input.nextLine().toUpperCase().trim();
       
        // check if study mode is empty and check if value is FT or PT, if didnt pass call the method again
        if(studyMode.equals("FT") == false && studyMode.equals("PT") == false )this.setStudyMode();
    }
    
    /**
     * void method to set number of mobule
     */
    public void setNumberOfModule(){
        System.out.println("Enter number of module (range 1-6): ");
        numberOfModule = input.nextInt();
       
        // check if number of module is empty and check if value is in range of 1 and 6, if didnt pass call the method again
        if(numberOfModule < 1 || numberOfModule > 6 )this.setNumberOfModule();
    }
    
    /**
     * void method to set level year
     * */
    public void setLevelYear(){
        System.out.println("Enter level year (range 1-4): ");
        levelYear = input.nextInt();
       
        // check if number of year is empty and check if value is in range of 1 and 4, if didnt pass call the method again
        if(levelYear < 1 || levelYear > 4 )this.setLevelYear();
    }
    
    
    
    /**
     * Boolen Method to check dob format
     * @return 
     */
    public boolean checkDob(){
        
        // Check if valu inpu has number and / alone
        if(!Pattern.matches("^[0-9\\/s]*$", dob)){
            System.out.println("Invalid Input");
            return false;
        }
        
        int presentYear = Year.now().getValue();                    // Get Present Year
        String presentDate = LocalDate.now().toString();            // Get Present Date and convert to String
        String[] split = dob.split("\\/");                     // Split dob
        
        // check if split array not equal to 3
        if(split.length != 3) {
            System.out.println("Invalid Date format");
            return false;
        }                         
        
        
        int day = Integer.parseInt(split[0]);
        int month = Integer.parseInt(split[1]);
        int year = Integer.parseInt(split[2]);
        
        // Check if dd is in range of 1 and 31
        if(day < 1 || day > 31 ) {
            System.out.println("Date day not valid, should be in range of 01 - 31");
            return false;
        }
        
        // Check if mm value is in range of 0 and 12
        if(month < 1 || month > 12 ) {
            System.out.println("Date month not valid, should be in range of 01 - 12");
            return false;
        }
        
        //check day for February
        if(month == 2 && day > 29){
            System.out.println("Date not valid, dd should be in range 01-29 for Month ("+month+")");
            return false;
        }
        
        // check day for 30 in month
        if((month == 4 || month == 6 || month == 9 || month == 11) && day > 30){
            System.out.println("Date not valid, dd should be in range 01-30 for Month ("+month+")");
            return false;
        }
            
        
        // check if yyyy is in range of College or University minimum year of 16(average year)
        if(Period.between(LocalDate.of(year, month, day), LocalDate.now()).getYears() < 16 ) {
            System.out.println("Age not valid, age should not be lesser than 16");
            return false;
        }
        
        return true;
        
    }
    
    /**
     * Method to return Age
     * @param dob
     * @return 
     */
    public int getAge(String dob){
        
        String[] split = dob.split("\\/");                     // Split dob
        
        // check if split array not equal to 3
        if(split.length != 3) {
            return 0;
        }                         
        
        
        int day = Integer.parseInt(split[0]);
        int month = Integer.parseInt(split[1]);
        int year = Integer.parseInt(split[2]);
        
        // return the years after comparing the dob and present date
        return Period.between(LocalDate.of(year, month, day), LocalDate.now()).getYears(); 
                
    }
    
    
    /**
     * void method to perform student registration
     */
    public void studentRegistration(){
        
        System.out.println("");
        if(this.checkStudent() < MAX_STUDENT ){     // check and print error if student in List is 20
            this.setFirstName();                    // calling method to set first Name
            this.setLastName();                     // calling method to set last name
            this.setDOB();                          // calling method to set date of birth
            this.setGender();                       // calling method to set gender
            this.setStudyMode();                    // calling method to study mode
            this.setLevelYear();                    // calling method to set mode year
            this.setNumberOfModule();               // calling method to set number of module
            this.addStudent();                      // calling method to to add student tp List                    // return to menu
        }else{
        System.out.println("\n===============================================================");
        System.out.println("ERROR: Sorry, We wont be taking in any registration. Try again later.");
        System.out.println("===============================================================\n");
        home.start();   }
        
    }
    
    /**
     * Void method to add Student to List
     */
    public void addStudent(){
        
        this.confirmExistingStudent();          // calling method to check if name exist
        
        if(firstName.isEmpty() == false && lastName.isEmpty() == false){ // checking if first name and last name not empty before adding
            // add string of student data to Student List
            studentArray.add(firstName+" "+lastName+","+dob+","+gender+","+studyMode+","+levelYear+","+numberOfModule);
        }
        System.out.println("\n===============================================================");
        System.out.println("SUCCESS: Student added successfully.");
        System.out.println("===============================================================\n");
        this.listOfStudent();                   // calling method to print list of available student in List          
        //home.start();
    }
    
    /**
     * Void Method to Populate Course Details from CourseDetails file
     */
    public void populateCourseDetails(){
        try {
            // set file reader class by using BufferedReader to read the file
            reader = new BufferedReader(new FileReader(courseFile));
            int lineSize = reader.read();       // set number of lines in the file
            
            if(lineSize == -1) {                // check if no lines or data in the file
                
                // This help to recognize the system doesnt have any data, and it will help request for course name
                System.out.println("\n===============================================================");
                System.out.println("This is a new Program for Course Enrolment");
                System.out.print("Kindly enter the Course you wish to setup enrolment for: ");
                courseName = input.nextLine();          // accepting course name
                
                while(courseName.isEmpty() == true) {   // check if course name input is empty
                    System.out.print("Kindly re-enter the Course name: ");
                    courseName = input.nextLine();
                    break;
                }
                
                // course name has been collected, now initiate program
                System.out.println("");
                System.out.println("===========================================================================");
                System.out.println("Welcome to "+courseName+" course enrolment platform."); 
                
                home.start();                           // calling method to start menu
            }
            
            // This initiate if course data was already set, while the program was launch 
            String line = reader.readLine();            // set lines value
            int count = 0;                              // set count of line to Zero
            
            // loop each line to be written on the screen
            while (line != null && line.isEmpty() == false) {
                    System.out.println(line);
                    if(count == 1) heading = line;
                    line = reader.readLine();
                    count++;
            }
            reader.close();
        
        }catch(IOException e){
            e.getMessage();
            System.out.println("\n===============================================================");
            System.out.println("ERROR: Course File Could not be accessed");
            System.out.println("===============================================================\n");
            this.populateCourseDetails();
        }
    }
    
    /**
     * Void method to populate student record into Student List
     */
    public void populateStudent(){
        
        try {
            // set file reader
            reader = new BufferedReader(new FileReader(studentFile));
            String line = reader.readLine();        // initiate firstg line and store into line as string
            
            //loop each line from file until line is null or line is empty
            while (line != null && line.isEmpty() == false) {
                    studentArray.add(line);
                    line = reader.readLine();
            }
            reader.close();                         // close file
        
        }catch(IOException e){
            e.getMessage();
            System.out.println("\n===============================================================");
            System.out.println("ERROR: File could not be populated.");
            System.out.println("===============================================================\n");
            home.start();
        }
        
    }
    
    
    /**
     * Void method to write data into StudentDetails and CourseDetails file
     */
    public void writeToFile(){
        try{
           
            File toDeleteStudent = new File(studentFile);           // set student file
            toDeleteStudent.delete();                                      // delete previous file
            File toDeleteCourse = new File(courseFile);             // set course file
            toDeleteCourse.delete();                                        // delete course file
            
            System.out.println("");
            System.out.println("-> Writing Student Data ...");
            File file = new File(studentFile);                      // set new student file
            
            FileWriter write = new FileWriter(file, true);           // set file to be appended
            
            //loop through student List to add each index in List into file
            for(int i = 0; i < studentArray.size(); i++){
                if(studentArray.get(i).toString().isEmpty() == false){
                    write.append(studentArray.get(i).toString()+"\n");
                }
            }
            System.out.println("Done");
            write.close();                                                  // close student file
            
            
            File file2 = new File(courseFile);                      // set new course details file
            FileWriter courseWrite = new FileWriter(file2, true);// set to be able to append data
            
            System.out.println("-> Writing Course Data ...");
            
            //start writing course report and student analytic report into course details file
            courseWrite.append("===========================================================================\n");
            if(heading == null){                        // check if null student heading set
                courseWrite.append("Welcome to "+courseName+" course enrolment platform.\n"); // write new heading
            }else{
                courseWrite.append(heading+"\n");       // write heading set previous when data loaded
            }
            int studentCount = studentArray.size();

            this.doReport();                                    // call method to initiate student analytic report
            float male = (float) noOfMale/studentCount;         // calculate male student percentage
            float female = (float) noOfFemale/studentCount;     // calculate female student percentage
            courseWrite.append("Last saved student report shown below\n");
            courseWrite.append("Total Number of Student: "+studentCount+"\n");
            courseWrite.append("Percentage of Male: "+String.format("%.2f", male * 100)+"%\n");
            courseWrite.append("Percentage of Female: "+String.format("%.2f", female * 100)+"%\n");
            courseWrite.append("Total Number of Full Time (FT): "+noOfFT+"\n");
            courseWrite.append("Total Number of Part Time (PT): "+noOfPT+"\n");
            System.out.println("Done");
            
            courseWrite.close();                                // close file after writing data to file
            
            
            System.out.println("\n===============================================================");
            System.out.println("Student information in array written to Course Details.");
            System.out.println("Bye Bye.");
            System.out.println("===============================================================\n");
            
            System.exit(0);                 //exit the program after writing data
            
        }catch(IOException e){
            e.getMessage();
            System.out.println("\n===============================================================");
            System.out.println("ERROR: File could not be written.");
            System.out.println("===============================================================\n");
            home.start();
            
        }
        
        
        
    }
    
    
    /**
     * Void method to show all available student in array in a table
     */
    public void listOfStudent(){
        System.out.println("");
        
        // display error if student information in array is less that 1
        if(studentArray.size() < 1){
            System.out.println("\n===============================================================");
            System.out.println("ERROR: No student record available.");
            System.out.println("===============================================================\n");
            home.start();
        }
        
        int count = studentArray.size();
        
        // set up format to create table and for name display
        String format = "|%1$-25s|%2$-25s|%3$-10s|%4$-15s|%5$-15s|%6$-10s|%7$-20s|\n";
        System.out.format(format, " NAMES ", " DATE OF BIRTH (AGE) ", " GENDER ", " STUDY MODE ", " LEVEL YEAR ", " MODULE ", " FEE ");
        
        // loop through student List 
        for(int i = 0; i < count; i++){
            if(studentArray.get(i).toString().isEmpty() == false){          // check if index of the List is not null or empty
                // create a delimter split for the data in array to be splitted
                
                String[] studentDataArr = studentArray.get(i).toString().split(",", 6);
                fee = 0;
                
                // check if student is FT to help calculate fee
                if(studentDataArr[3].equals("FT")) {
                    // call FullTime constructor to set value
                    FullTime fulltime = new FullTime(Integer.parseInt(studentDataArr[4]));
                    fee = fulltime.schoolFee();                 // calculate fee
                }else {
                    // call PartTime constructor to set value
                    PartTime parttime = new PartTime(Integer.parseInt(studentDataArr[5]));
                    fee = parttime.schoolFee();                 // calculate fee
                }
                
                //Print out student List
                System.out.format(format, " "+studentDataArr[0]+" ", " "+studentDataArr[1]+" ("+this.getAge(studentDataArr[1])+") ", " "+studentDataArr[2]+" ", " "+studentDataArr[3]+" ", " "+studentDataArr[4]+" ", " "+studentDataArr[5]+" ", "£"+String.format("%.2f", fee)+"" );
            }
            
        }
        
        String ex[] = { "==", "==", "==", "==", "==", "==", "==" };
        System.out.format(String.format(format, (Object[]) ex));
        
        this.doReport();                            // call method to perform student anaylytic report
        
        
        //calculate student alaytics report,
        float male = (float) noOfMale/count;          // percentage of Male student
        float female = (float) noOfFemale/count;       // percenateg of Femail student
        
        System.out.println("");
        System.out.println("Total Number of Student: "+count);  // Total student count
        System.out.println("Percentage of Male: "+String.format("%.2f", male * 100)+"%"); // Overall pertage of Male Student
        System.out.println("Percentage of Female: "+String.format("%.2f", female * 100)+"%"); // Overall pertage of Male Student
        System.out.println("Total Number of Full Time (FT): "+noOfFT); // Total number of Full Time
        System.out.println("Total Number of Part Time (PT): "+noOfPT); // Total number of Part Time
        
    }
    
    
    /**
     * Void method to perform student analytics report, store data in encapsulated variable
     */
    public void doReport(){
        noOfMale = noOfFemale = noOfFT = noOfPT = 0; // initiate count basck to Zero
        
        //loop through Student List
        for(int i = 0; i < studentArray.size(); i++){
            if(studentArray.get(i).toString().isEmpty() == false){
                String[] studentDataArr = studentArray.get(i).toString().split(",", 6);
                fee = 0;
                if(studentDataArr[3].equals("FT")) noOfFT++;        // Increase value of Student is FT
                if(studentDataArr[3].equals("PT")) noOfPT++;        // Increase value of Student is PT
                if(studentDataArr[2].equals("M")) noOfMale++;       // Increase value of Student is Male
                if(studentDataArr[2].equals("F")) noOfFemale++;     // // Increase value of Student is Female           
            }
            
        }
    }
    
    
    /**
     * Void method to display Single student
     */
    public void showSingleStudent(){
        System.out.println("");
        this.setFirstName();                        // call method to set first name
        this.setLastName();                         // call method to set last name
        
        int count =0;
        int arrCount = studentArray.size();         // get student List count
        
        System.out.println("\n");
        String format = "|%1$-25s|%2$-25s|%3$-10s|%4$-15s|%5$-15s|%6$-10s|%7$-10s|\n";
        System.out.format(format, " NAMES ", " DATE OF BIRTH (AGE)", " GENDER ", " STUDY MODE ", " LEVEL YEAR ", " MODULE ", " FEE ");
        
        for(int i = 0; i < arrCount; i++){
            String[] arr = studentArray.get(i).toString().split(",", 6);
            if(arr[0].equals(firstName+" "+lastName) == true){
                count++;
                
                fee = 0;
                if(arr[3].equals("FT")) {
                    FullTime fulltime = new FullTime(Integer.parseInt(arr[4]));
                    fee = fulltime.schoolFee();
                }else {
                    PartTime parttime = new PartTime(Integer.parseInt(arr[5]));
                    fee = parttime.schoolFee();
                }
                
              System.out.format(format, " "+arr[0]+" ", " "+arr[1]+" ("+this.getAge(arr[1])+") ", " "+arr[2]+" ", " "+arr[3]+" ", " "+arr[4]+" ", " "+arr[5]+" ", "£"+String.format("%.2f", fee)+"" );
            }
        }
        String ex[] = { "==", "==", "==", "==", "==", "==", "==" };
        System.out.format(String.format(format, (Object[]) ex));


        System.out.println("\n===============================================================");
        if(count > 0){System.out.println("SUCCESS: Data retrieved");}
        else{System.out.println("ERROR: No student record");}
        System.out.println("===============================================================\n");
       
        count = 0;
        
        
    }
    
    /**
     * Void method to check if Student exist
     */
    public void confirmExistingStudent(){
        System.out.println("");
        int count = studentArray.size();        // get number of data in student List
        
        // loop through student to check if name exist
        for(int i = 0; i< count; i++){
            // split into array object stored in the list
            String[] studentDataArr = studentArray.get(i).toString().split(",", 6);
            
            //Concatenate first name and last name
            // check if name exist while looping
            if(studentDataArr[0].equals(firstName+" "+lastName) == true){
                // show error if name exist
                System.out.println("\n===============================================================");
                System.out.println("ERROR: Sorry, Student with names ( "+ studentDataArr[0] +") exist.");
                System.out.println("===============================================================\n");
                home.start();               // return back to menu
            }  
        }
        
              
    }
    
    /**
     * Method to get student total number
     * @return 
     */
    public int checkStudent(){
        return studentArray.size();
    }
    
    /**
     * void method to delete student
     */
    public void deleteStudent(){
        System.out.println("");
        this.setFirstName();                        // call method to set first name
        this.setLastName();                         // call method to set last name
        
        String fullName = firstName+" "+lastName;   // concantenate first name and last name
        int found =0;
        int count = studentArray.size();            // total number of syudent in List
        // loop through List to check for name
        for(int i = 0; i < count; i++){
            String[] arr = studentArray.get(i).toString().split(",", 6);
            if(arr[0].equalsIgnoreCase(fullName.toUpperCase())){  // check if name exist
                studentArray.remove(i);                    // remove index record from List
                found++;                                        // increase found
                break;
            }
        }
        
        // print data delete successfully when found greater than 0
        if(found > 0){
            System.out.println("");
            System.out.println("\n===============================================================");
            System.out.println("SUCCESS: Student with name ("+fullName+") was deleted.");
            System.out.println("===============================================================\n");
            System.out.println("");
            
        }else{
            // print data could not be delete, no name found
            System.out.println("");
            System.out.println("\n===============================================================");
            System.out.println("ERROR: Student with name ("+fullName+") could not be found.");
            System.out.println("===============================================================\n");
            System.out.println("");
        }
        found=0;                                    // reinitiate found back to default
        
        //list the remaining student information present in the list
        this.listOfStudent();       
        
    }
 
}
