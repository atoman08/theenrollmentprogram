/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com102group20.theenrollmentregister;

/**
 *
 * @author atoman
 * * Group Member: Akinpelu Taofik Opeyemi (B00902238) 
 * and Adebayo Rasheed Ayomide (B00857088)
 */
import java.util.*;
import java.io.*;
public class PartTime extends Student  {
    
    private int price = 750;                    //initiate price for each module
    private int moduleNum;                      // set variable for student module
    
    //constructor for PartTime Class
    PartTime( int inputModuleNum){
        moduleNum = inputModuleNum;             // set module number for student (encpasulation)
    }
    
    // method to perform fee calculation
    public int schoolFee(){
        return moduleNum * price;               // calculate and return school fee
    }
}
