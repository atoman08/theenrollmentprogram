/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com102group20.theenrollmentregister;

/**
 *
 * @author atoman
 * Group Member: Akinpelu Taofik Opeyemi (B00902238) 
 * and Adebayo Rasheed Ayomide (B00857088)
 */
import static com102group20.theenrollmentregister.Student.home;
import java.util.*; 
import java.io.*;
import java.nio.file.Paths;
public class TheEnrollmentRegister {
    
    // set scanner object as input
    Scanner input = new Scanner(System.in);   
    //set student object from studnet constructor in student class
    public static final Student student = new Student("", "", 'a', "", "", 0, 0);
    
    /**
     * Main Method
     * @param args 
     */
    public static void main(String[] args) {
        
        TheEnrollmentRegister  trigger = new TheEnrollmentRegister();       // initiate trigger as an object from a copy of the class
        trigger.populateData();                                             // trigger the class method to populate Data
        trigger.start();                                                    // trigger the class method to initiate start method
        
    } 
    
    /**
     * Void method to trigger populate method
     */
    public void populateData(){
        student.populateCourseDetails();                                    // call method to populate course details from student class using student ojcet created
        student.populateStudent();                                          // call method to populate student details from student class using student ojcet created
    }
    
    /**
     * Void method to start menu
     */
    public void start(){
        this.menu();
    }
    
    /**
     * Void method to show menu
     */
    public void menu(){
        
        //Display Menu options
        System.out.println("");
        System.out.println("Follow the below to perfrom an operation: ");
        System.out.println("> [a] New registration");
        System.out.println("> [b] View all registration");
        System.out.println("> [c] View a student information ");
        System.out.println("> [d] Delete a student information ");
        System.out.println("> [x] Exit the program (All data will be written to file) ");
        System.out.println("Thanks ");
        
        
        int value = Character.toLowerCase(input.next().charAt(0));    // get first data of input         
        if(value == 'a'){student.studentRegistration(); this.start();}        // open student registration method in Student class
        else if(value == 'b'){student.listOfStudent(); this.start();}           // open student list method in Student class
        else if(value == 'c'){student.showSingleStudent(); this.start();}       // open show and search student method in Student class
        else if(value == 'd'){student.deleteStudent(); this.start();}           // open delet studnet method in Student class
        else if(value == 'x'){student.writeToFile();}                           // open method to save and write student details and course details in Student class
        else{
            System.out.println("\n===============================================================");
            System.out.println("ERROR: Invalid Input.");
            System.out.println("===============================================================\n");
            this.start();
        }  
    }
    
    
    
}
