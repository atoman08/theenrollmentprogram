/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com102group20.theenrollmentregister;

/**
 *
 * @author atoman
 * * Group Member: Akinpelu Taofik Opeyemi (B00902238) 
 * and Adebayo Rasheed Ayomide (B00857088)
 */
public class FullTime extends Student {
   
    private int modulePrice = 5000 ;        // initiate module price
    private int thirdYear = 2500;           // initiate module price for 3 year
    private int studentLevel;               // set variable for studentLevel
       
    // constructor for FullTime Class
    FullTime (int level){
                                            // set student level
        studentLevel = level;
    }
   
    //method to calculate and return fee
    public int schoolFee(){
        
        if(studentLevel == 3){              // check if student level is 3
            return thirdYear;               // return fee amount for third year
        }else{
            return modulePrice;             // return module fee for others
        }
    }
    
}
